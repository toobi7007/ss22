#include <iostream>
#include <thread>
#include <chrono>

void helo() {
  std::cout << "HEI" << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
}

int main() {
  std::thread t1(helo);
  t1.join();
  return 0;
}
