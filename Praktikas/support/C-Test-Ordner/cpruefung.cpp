#include <iostream>
#include <thread>
#include <mutex>
std::mutex mut;

void fn() {
  mut.lock();
  std::cout << "Hallo aus dem Mutex gesicherten Thread." << std::endl;
  std::this_thread::sleep_for(std::chrono::seconds(5));
  mut.unlock();
}

int main() {
  std::thread t(fn);
  std::this_thread::sleep_for(std::chrono::seconds(2));
  mut.lock();
  std::cout << "Guten Abend aus main." << std::endl;
  mut.unlock();

  t.join();
  return 0;
}
