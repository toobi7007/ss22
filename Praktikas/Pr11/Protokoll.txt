1. Client -> Server
Client sendet Zieldatei + Statusmeldung.

2. Server -> Client
Server sendet erneut Zieldatei + Statusmeldung um zu schauen,
ob mit dem Kopieren begonnen werden kann.
   0 -> alles ok
   1 -> Fuer die Zieldatei besteht kein Schreibrecht
   2 -> Zieldatei ist ein Verzeichnis
   3 -> Fehler beim Oeffnen der Zieldatei
   4 -> Fehler beim Erstellen der Zieldatei
Zum erstellen der Datei werden zusaetzlich die Meta Informationen
geschickt.

3. Client -> Server
Client sendet Stueck fuer Stueck die Datei, die Anzahl der Pakete, die
Anzahl der bereits gesendeten Pakete, ob von Client Seite aus alles okay
ist und als letztes wie viel Bytes geschrieben werden muessen.
