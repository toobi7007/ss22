#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stddef.h>
#include <signal.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

#include "data.h"

int sock_fd;

void exithandler() {
  if(close(sock_fd) < 0) {
    perror("close");
    _exit(EXIT_FAILURE);
  }
  
  if(unlink(SOCKET_NAME) < 0) {
    perror("unlink");
    _exit(EXIT_FAILURE);
  }
}

void sighandler(int signo) {
  exithandler();
  _exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
  if(argc != 1) {
    fprintf(stderr, "Aufruf: ./server");
    exit(EXIT_FAILURE);
  }

  int sock_fd;
  int k_sock_fd;
  int addresse_len;
  socklen_t client_a_len;
  struct sockaddr_un server_a, client_a;
  struct sigaction old, new;

  if((sock_fd = socket(PF_LOCAL, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }

  server_a.sun_family = AF_LOCAL;
  strcpy(server_a.sun_path, SOCKET_NAME);
  addresse_len = sizeof(server_a);
  unlink(SOCKET_NAME);

  /* Addresse binden */
  if(bind(sock_fd, (struct sockaddr *)&server_a,
	  addresse_len) <0 ) {
    perror("bind");
    exit(EXIT_FAILURE);
  }

  /* Sicherstellen das jeder Nutzer das Socket verwenden kann */
  if(chmod(SOCKET_NAME, 0777) < 0) {
    perror("chmod");
    exit(EXIT_FAILURE);
  }

  /* Warteschlange initialisieren */
  if(listen(sock_fd, 5) < 0) {
    perror("liste ");
    exit(EXIT_FAILURE);
  }

  /* Exit-Handler initialisieren */
  atexit(exithandler);

  /* Signalbehandlung vorbereiten und installieren */
  sigemptyset(&(new.sa_mask));
  new.sa_handler = sighandler;
  new.sa_flags = 0;

  if(sigaction(SIGINT, &new, &old) < 0) {
    perror("sigaction");
    exit(EXIT_FAILURE);
  }

  struct anfrage anf;
  int nread;

  /* Beginn der Serverschleife -------------------- */
  while(1) {
    client_a_len = sizeof(struct sockaddr_un);

    /* Auf den Auftrag warten */
    if((k_sock_fd = accept(sock_fd, (struct sockaddr *)&client_a,
			   &client_a_len)) < 0) {

      perror("accept");
      exit(EXIT_FAILURE);
    }

    /* Auftrag lesen */
    if((nread = read(k_sock_fd, &anf, sizeof(struct anfrage))) < 0) {
      perror("read");
      exit(EXIT_FAILURE);
    }

    int fd_writingFile;
    // Pruefen ob die Datein bereits existiert
    anf.status = 0;
    if(access(anf.path, F_OK) == 0) { 
      // Datei wird ueberprueft ob man sie "benutzen" kann
      if(access(anf.path, W_OK) != 0) {
	anf.status = 1;
      } else if((fd_writingFile = open(anf.path, O_WRONLY)) < 0) {
	if(errno == EISDIR) {
	  anf.status = 2;
	} else {
	  anf.status = 3;
	}
      }
    } else {
      // Datei wird erstellt
      if((fd_writingFile = creat(anf.path, anf.info.st_mode)) < 0) {
	anf.status = 4;
      }
    }

    /* Status zurueckgeben -> 0 = alles ok */
    if((nread = write(k_sock_fd, &anf, sizeof(struct anfrage))) < 0) {
      perror("write");
      exit(EXIT_FAILURE);
    }

    struct packet p;
    do {
      // Lesen des Inhalts der Datei
      if((nread = read(k_sock_fd, &p, sizeof(struct packet))) < 0) {
	perror("read");
	exit(EXIT_FAILURE);
      }

      // Schreiben des Inhalts in die Zieldatei
      if(write(fd_writingFile, p.inhalt, p.groesse) < 0) {
	perror("write");
        exit(EXIT_FAILURE);
      }
    } while(p.anzahl_pakete -1 != p.abgearbeitete_pakete);
  }
  /* Ende der Serverschleife -------------------- */

  /* Schliessen des Sockets */
  if(close(sock_fd) < 0) {
    perror("close");
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}
