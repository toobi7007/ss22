#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stddef.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "data.h"

int main(int argc, char **argv) {
  if(argc != 3) {
    fprintf(stderr, "Aufruf: ./client [QUELLDATEI] [ZIELDATEI] \n");
    exit(EXIT_FAILURE);
  }

  int sock_fd;
  int addresse_len;
  struct sockaddr_un server_addresse;

  if((sock_fd = socket(PF_LOCAL, SOCK_STREAM, 0)) < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }

  server_addresse.sun_family = AF_LOCAL;
  strcpy(server_addresse.sun_path, SOCKET_NAME);
  addresse_len = sizeof(server_addresse);

  struct anfrage anf;
  int fd_readingFile;

  struct anfrage ant;
  int nread;
    
  /* Zu lesende Datei ueberpruefen */
  if((fd_readingFile = open(argv[1], O_RDONLY)) < 0) {
    perror("open");
    exit(EXIT_FAILURE);
  }

  /* Holen der Meta Informationen der Datei */
  if(fstat(fd_readingFile, &anf.info) < 0) {
    perror("fstat");
    exit(EXIT_FAILURE);
  }

  /* Pruefen ob es eine regulaere Datei ist */
  if(S_ISREG(anf.info.st_mode) == 0) {
    fprintf(stderr, "Die Quelldatei ist keine requlaere Datei.\n");
    exit(EXIT_FAILURE);
  }

  /* Verbinden mit dem Socket */
  if(connect(sock_fd, (struct sockaddr *)&server_addresse,
	     addresse_len) <0) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  // Setzen des Paths fuer die Zieldatei
  strcpy(anf.path, argv[2]);

  // Status das von Client Seite aus alles OK ist
  anf.status = 0;

  // Anfrage ins Socket schreiben
  if((nread = write(sock_fd, &anf,
		    sizeof(struct anfrage))) < 0) {
    perror("write");
    exit(EXIT_FAILURE);
  }

  // Antwort von Server warten
  if((nread = read(sock_fd, &ant,
		   sizeof(struct anfrage))) < 0) {
    perror("read");
    exit(EXIT_FAILURE);
  }

  // Status auswerten -> 0 ist alles okay
  switch(ant.status) {
  case 1:
    fprintf(stderr, "Server: Fuer die Zieldatei besteht kein Schreibrecht\n");
    exit(EXIT_FAILURE);
    break;

  case 2:
    fprintf(stderr, "Server: Zieldatei ist ein Verzeichnis.\n");
    exit(EXIT_FAILURE);
    break;

  case 3:
    fprintf(stderr, "Server: Fehler beim Oeffnen der Zieldatei.\n");
    exit(EXIT_FAILURE);
    break;

  case 4:
    fprintf(stderr, "Server: Fehler beim Erstellen der Zieldatei.\n");
    exit(EXIT_FAILURE);
    break;
  }

  // Ausrechnen der Anzahl zu sendenen Pakete
  const blksize_t bufsize = anf.info.st_blksize;
  int wdh = (int)anf.info.st_blocks;
  if(bufsize != BUF) wdh = wdh * (bufsize/BUF);

  // Packet fuer die Datenuebertragung vorbereiten
  struct packet package;
  package.anzahl_pakete = wdh;

  // Stueck fuer Stueck kopieren der Daten
  for(int kopieren = 1; kopieren != wdh; ++kopieren) {
    package.abgearbeitete_pakete = kopieren;
    if((package.groesse = read(fd_readingFile, package.inhalt, BUF)) < 0) {
      perror("read");
      exit(EXIT_FAILURE);
    }

    if((nread = write(sock_fd, &package,
		      sizeof(struct packet))) < 0) {
      perror("write");
      exit(EXIT_FAILURE);
    }
  }

  /* Schliessen des Sockets */
  if(close(sock_fd) < 0) {
    perror("close");
    exit(EXIT_FAILURE);
  }

  /* Schliessen der zu lesenden Datei */
  if(close(fd_readingFile) < 0) {
    perror("close");
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}
