#ifndef DATA_H
#define DATA_H

#define SOCKET_NAME "/tmp/socket"
#define BUF 1024

#include <sys/stat.h>

struct anfrage {
  char path[100]; // Pfad der Zieldatei
  int status; // Status der Rueckgabe
  struct stat info; // Meta Inforamtionen der Quelldatei
};

struct packet {
  char inhalt[BUF];
  int groesse;
  int anzahl_pakete;
  int abgearbeitete_pakete;
  int status;
};

#endif
